Marked In HTML
====

在`HTML`中书写`Markdown`，同时适用于[`impress.js`](https://github.com/impress/impress.js)

### 起步
```html
<!DOCTYPE html>
<html lang="zh">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <link rel="stylesheet" href="https://cdn.bootcss.com/highlight.js/9.12.0/styles/atom-one-light.min.css" />
  <link rel="stylesheet" href="https://cdn.bootcss.com/github-markdown-css/2.8.0/github-markdown.min.css" />
  <title>marked in html</title>
</head>
<body>
  <template type="markdown">
    欢迎使用 Marked In HTML ！
    ====
  </template>
</body>
<script src="https://cdn.bootcss.com/marked/0.3.6/marked.min.js" charset="utf-8"></script>
<script src="https://cdn.bootcss.com/highlight.js/9.12.0/highlight.min.js" charset="utf-8"></script>
<script src="https://cdn.bootcss.com/highlight.js/9.12.0/languages/javascript.min.js" charset="utf-8"></script>
<script src="../dist/marked-in-html.min.js" charset="utf-8"></script>
<script type="text/javascript">
  markedInHtml.init()
</script>
</html>

```

### 在线示例

1. [简易示例一](http://hungtcs.oschina.io/marked-in-html/examples/)
1. [简易示例二](http://hungtcs.oschina.io/marked-in-html/examples/simple.html)
1. [配合`impress.js`](http://hungtcs.oschina.io/marked-in-html/examples/impress-simple.html#/step-1)
